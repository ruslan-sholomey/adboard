package com.geekhub.web;

import com.geekhub.model.Advert;
import com.geekhub.service.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AdvertController {

    @Autowired
    private AdvertService advertService;

    @RequestMapping(value = "/newAdvert", method = RequestMethod.GET)
    public String add(Model model){
        model.addAttribute("advert", new Advert());
        return "newAdvert";
    }

    @RequestMapping(value = "/newAdvert", method = RequestMethod.POST)
    public String add(@ModelAttribute("advert") Advert advert, BindingResult result){
        if (result.hasErrors()){
            return "newAdvert";
        }
        advertService.addAdvert(advert);
        return "redirect:/adBoard";
    }


    @RequestMapping(value = "/adBoard", method = RequestMethod.GET)
    public String home(Model model){
        model.addAttribute("adverts", advertService.getAllAdvert());
        return "adBoard";
    }

    @RequestMapping(value = "/adBoard/details/{id}", method = RequestMethod.GET)
    public String advertDetails(@PathVariable("id") Long id, Model model){
        model.addAttribute("advert", advertService.getAdvertById(id));
        return "advertDetails";
    }
}
