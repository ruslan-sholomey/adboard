package com.geekhub.repository;

import com.geekhub.model.Advert;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AdvertRepository extends PagingAndSortingRepository<Advert, Long> {
}
