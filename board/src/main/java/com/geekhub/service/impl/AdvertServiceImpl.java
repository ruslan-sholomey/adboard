package com.geekhub.service.impl;

import com.geekhub.model.Advert;
import com.geekhub.repository.AdvertRepository;
import com.geekhub.service.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AdvertServiceImpl implements AdvertService {

    @Autowired
    private AdvertRepository advertRepository;

    @Override
    public void addAdvert(Advert advert) {
        advertRepository.save(advert);
    }

    @Override
    public List<Advert> getAllAdvert() {
        return (List<Advert>) advertRepository.findAll();
    }

    @Override
    public Advert getAdvertById(Long id) {
        return advertRepository.findOne(id);
    }
}
