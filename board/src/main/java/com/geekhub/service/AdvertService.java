package com.geekhub.service;

import com.geekhub.model.Advert;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AdvertService {
    void addAdvert(Advert advert);
    Advert getAdvertById(Long id);
    List<Advert> getAllAdvert();
}
