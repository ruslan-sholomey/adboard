drop table if EXISTS advert;
CREATE TABLE advert(
                id   INT NOT NULL AUTO_INCREMENT,
                title VARCHAR (20) NOT NULL,
                price DOUBLE (20) NOT NULL,
                firstName VARCHAR (20) NOT NULL,
                lastName VARCHAR (20) NOT NULL,
                phoneNumber VARCHAR (20) NOT NULL,
                description VARCHAR (255) NOT NULL,
                PRIMARY KEY (id)
        );